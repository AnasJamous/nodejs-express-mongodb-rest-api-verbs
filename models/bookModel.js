const mongoose = require("mongoose");

// Creating a schema and destructioring it from mongoose:
const { Schema } = mongoose;

// Book Model:
const bookModel = new Schema({
  title: { type: String },
  author: { type: String },
  genre: { type: String },
  read: { type: Boolean, default: false },
});

// exporting bookModel as Book:
module.exports = mongoose.model("Book", bookModel);
