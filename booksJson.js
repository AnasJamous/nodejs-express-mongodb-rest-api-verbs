db.books.insert([
  {
    title: "Title one",
    genre: "Historical Fiction",
    author: "Anas Jamous",
    read: false,
  },
  {
    title: "Title two",
    genre: "Historical Fiction",
    author: "Anas Jamous",
    read: false,
  },
  {
    title: "Title three",
    genre: "Historical Fiction",
    author: "Anas Jamous",
    read: false,
  },
  {
    title: "Title four",
    genre: "Historical Fiction",
    author: "Anas Jamous",
    read: false,
  },
  {
    title: "Title five",
    genre: "Historical Fiction",
    author: "Anas Jamous",
    read: false,
  },
  {
    title: "Title Six",
    genre: "Historical Fiction",
    author: "Anas Jamous",
    read: false,
  },
]);
